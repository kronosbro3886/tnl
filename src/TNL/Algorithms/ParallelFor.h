// Copyright (c) 2004-2023 Tomáš Oberhuber et al.
//
// This file is part of TNL - Template Numerical Library (https://tnl-project.org/)
//
// SPDX-License-Identifier: MIT

#pragma once

#include <TNL/Algorithms/detail/ParallelFor1D.h>
#include <TNL/Algorithms/detail/ParallelFor2D.h>
#include <TNL/Algorithms/detail/ParallelFor3D.h>
#include <TNL/Containers/StaticArray.h>

/****
 * The implementation of ParallelFor is not meant to provide maximum performance
 * at every cost, but maximum flexibility for operating with data stored on the
 * device.
 *
 * The grid-stride loop for CUDA has been inspired by Nvidia's blog post:
 * https://devblogs.nvidia.com/parallelforall/cuda-pro-tip-write-flexible-kernels-grid-stride-loops/
 *
 * Implemented by: Jakub Klinkovsky
 */

namespace TNL {
namespace Algorithms {

// wrapper for expanding multi-index into multiple arguments
namespace detail {
template< typename Function, typename Device >
struct ParallelForExpandWrapper
{
   Function f;

   ParallelForExpandWrapper( Function f ) : f( f ) {}

   template< typename MultiIndex, typename... FunctionArgs >
   void
   operator()( const MultiIndex& i, FunctionArgs... args )
   {
      static_assert( MultiIndex::getSize() == 2 || MultiIndex::getSize() == 3 );
      if constexpr( MultiIndex::getSize() == 2 )
         f( i.x(), i.y(), args... );
      if constexpr( MultiIndex::getSize() == 3 )
         f( i.x(), i.y(), i.z(), args... );
   }
};

// stupid specialization to avoid a shitpile of nvcc warnings
// (nvcc does not like nested __cuda_callable__ and normal lambdas...)
template< typename Function >
struct ParallelForExpandWrapper< Function, Devices::Cuda >
{
   Function f;

   ParallelForExpandWrapper( Function f ) : f( f ) {}

   template< typename MultiIndex, typename... FunctionArgs >
   __cuda_callable__
   void
   operator()( const MultiIndex& i, FunctionArgs... args )
   {
      static_assert( MultiIndex::getSize() == 2 || MultiIndex::getSize() == 3 );
      if constexpr( MultiIndex::getSize() == 2 )
         f( i.x(), i.y(), args... );
      if constexpr( MultiIndex::getSize() == 3 )
         f( i.x(), i.y(), i.z(), args... );
   }
};
}  // namespace detail

// clang-format off

/**
 * \brief Parallel for loop for one dimensional interval of indices.
 *
 * \tparam Device specifies the device where the for-loop will be executed.
 *    It can be \ref TNL::Devices::Host, \ref TNL::Devices::Cuda or
 *    \ref TNL::Devices::Sequential.
 */
template< typename Device = Devices::Sequential >
struct [[deprecated("Use the TNL::Algorithms::parallelFor function instead.")]] ParallelFor
{
   /**
    * \brief Static method for the execution of the loop.
    *
    * \tparam Index is the type of the loop indices.
    * \tparam Function is the type of the functor to be called in each iteration
    *    (it is usually deduced from the argument used in the function call).
    * \tparam FunctionArgs is a variadic pack of types for additional parameters
    *    that are forwarded to the functor in every iteration.
    *
    * \param start is the left bound of the iteration range `[begin, end)`.
    * \param end is the right bound of the iteration range `[begin, end)`.
    * \param f is the function to be called in each iteration.
    * \param args are additional parameters to be passed to the function f.
    */
   template< typename Index, typename Function, typename... FunctionArgs >
   static void
   exec( Index start, Index end, Function f, FunctionArgs... args )
   {
      typename Device::LaunchConfiguration launch_config;
      exec( start, end, launch_config, f, args... );
   }

   /**
    * \brief Overload with custom launch configuration (which is ignored for
    * \ref TNL::Devices::Sequential).
    */
   template< typename Index, typename Function, typename... FunctionArgs >
   static void
   exec( Index start, Index end, typename Device::LaunchConfiguration launch_config, Function f, FunctionArgs... args )
   {
      detail::ParallelFor1D< Device >::exec( start, end, launch_config, f, args... );
   }
};

/**
 * \brief Parallel for loop for two dimensional domain of indices.
 *
 * \tparam Device specifies the device where the for-loop will be executed.
 *    It can be \ref TNL::Devices::Host, \ref TNL::Devices::Cuda or
 *    \ref TNL::Devices::Sequential.
 */
template< typename Device = Devices::Sequential >
struct [[deprecated("Use the TNL::Algorithms::parallelFor function instead.")]] ParallelFor2D
{
   /**
    * \brief Static method for the execution of the loop.
    *
    * \tparam Index is the type of the loop indices.
    * \tparam Function is the type of the functor to be called in each iteration
    *    (it is usually deduced from the argument used in the function call).
    * \tparam FunctionArgs is a variadic pack of types for additional parameters
    *    that are forwarded to the functor in every iteration.
    *
    * \param startX the for-loop iterates over index domain `[startX,endX) x [startY,endY)`.
    * \param startY the for-loop iterates over index domain `[startX,endX) x [startY,endY)`.
    * \param endX the for-loop iterates over index domain `[startX,endX) x [startY,endY)`.
    * \param endY the for-loop iterates over index domain `[startX,endX) x [startY,endY)`.
    * \param f is the function to be called in each iteration
    * \param args are additional parameters to be passed to the function f.
    *
    * The function f is called for each iteration as
    *
    * \code
    * f( i, j, args... )
    * \endcode
    *
    * where the first parameter is changing more often than the second one.
    */
   template< typename Index, typename Function, typename... FunctionArgs >
   static void
   exec( Index startX, Index startY, Index endX, Index endY, Function f, FunctionArgs... args )
   {
      typename Device::LaunchConfiguration launch_config;
      exec( startX, startY, endX, endY, launch_config, f, args... );
   }

   /**
    * \brief Overload with custom launch configuration (which is ignored for
    * \ref TNL::Devices::Sequential).
    */
   template< typename Index, typename Function, typename... FunctionArgs >
   static void
   exec( Index startX,
         Index startY,
         Index endX,
         Index endY,
         typename Device::LaunchConfiguration launch_config,
         Function f,
         FunctionArgs... args )
   {
      using MultiIndex = Containers::StaticArray< 2, Index >;
      const MultiIndex begin{ startX, startY };
      const MultiIndex end{ endX, endY };
      auto wrapper = detail::ParallelForExpandWrapper< Function, Device >( f );
      detail::ParallelFor2D< Device >::exec( begin, end, launch_config, wrapper, args... );
   }
};

/**
 * \brief Parallel for loop for three dimensional domain of indices.
 *
 * \tparam Device specifies the device where the for-loop will be executed.
 *    It can be \ref TNL::Devices::Host, \ref TNL::Devices::Cuda or
 *    \ref TNL::Devices::Sequential.
 */
template< typename Device = Devices::Sequential >
struct [[deprecated("Use the TNL::Algorithms::parallelFor function instead.")]] ParallelFor3D
{
   /**
    * \brief Static method for the execution of the loop.
    *
    * \tparam Index is the type of the loop indices.
    * \tparam Function is the type of the functor to be called in each iteration
    *    (it is usually deduced from the argument used in the function call).
    * \tparam FunctionArgs is a variadic pack of types for additional parameters
    *    that are forwarded to the functor in every iteration.
    *
    * \param startX the for-loop iterates over index domain `[startX,endX) x [startY,endY) x [startZ,endZ)`.
    * \param startY the for-loop iterates over index domain `[startX,endX) x [startY,endY) x [startZ,endZ)`.
    * \param startZ the for-loop iterates over index domain `[startX,endX) x [startY,endY) x [startZ,endZ)`.
    * \param endX the for-loop iterates over index domain `[startX,endX) x [startY,endY) x [startZ,endZ)`.
    * \param endY the for-loop iterates over index domain `[startX,endX) x [startY,endY) x [startZ,endZ)`.
    * \param endZ the for-loop iterates over index domain `[startX,endX) x [startY,endY) x [startZ,endZ)`.
    * \param f is the function to be called in each iteration
    * \param args are additional parameters to be passed to the function f.
    *
    * The function f is called for each iteration as
    *
    * \code
    * f( i, j, k, args... )
    * \endcode
    *
    * where the first parameter is changing the most often.
    */
   template< typename Index, typename Function, typename... FunctionArgs >
   static void
   exec( Index startX, Index startY, Index startZ, Index endX, Index endY, Index endZ, Function f, FunctionArgs... args )
   {
      typename Device::LaunchConfiguration launch_config;
      exec( startX, startY, startZ, endX, endY, endZ, launch_config, f, args... );
   }

   /**
    * \brief Overload with custom launch configuration (which is ignored for
    * \ref TNL::Devices::Sequential).
    */
   template< typename Index, typename Function, typename... FunctionArgs >
   static void
   exec( Index startX,
         Index startY,
         Index startZ,
         Index endX,
         Index endY,
         Index endZ,
         typename Device::LaunchConfiguration launch_config,
         Function f,
         FunctionArgs... args )
   {
      using MultiIndex = Containers::StaticArray< 3, Index >;
      const MultiIndex begin{ startX, startY, startZ };
      const MultiIndex end{ endX, endY, endZ };
      auto wrapper = detail::ParallelForExpandWrapper< Function, Device >( f );
      detail::ParallelFor3D< Device >::exec( begin, end, launch_config, wrapper, args... );
   }
};

// clang-format on

}  // namespace Algorithms
}  // namespace TNL
